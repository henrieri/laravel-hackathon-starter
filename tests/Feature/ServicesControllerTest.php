<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ServicesControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_api_call()
    {


        $apiToken = User::where('first_name', 'Olari')->first()->api_token;



        $response = $this->get('/services', [
            'Authorization' => 'Bearer '.  $apiToken
        ]);

        dd($response->json());
    }
}

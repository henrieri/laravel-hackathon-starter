<?php

namespace Henrieri\HackathonStarter;

use Arcanedev\LogViewer\LogViewerServiceProvider;
use Illuminate\Support\ServiceProvider;
use Laracasts\Generators\GeneratorsServiceProvider;
use Mpociot\LaravelTestFactoryHelper\TestFactoryHelperServiceProvider;
use Plank\Metable\MetableServiceProvider;
use Asvae\ApiTester\ServiceProvider as ApiTesterServiceProvider;

class HackathonStarterServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'henrieri');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'henrieri');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/hackathonstarter.php', 'hackathonstarter');

        // Register the service the package provides.
        $this->app->singleton('hackathonstarter', function ($app) {
            return new HackathonStarter;
        });
        if ($this->app->isLocal()) {
            $this->app->register(GeneratorsServiceProvider::class);
        }

        $this->app->register(MetableServiceProvider::class);
        $this->app->register(TestFactoryHelperServiceProvider::class);
        $this->app->register(ApiTesterServiceProvider::class);
        $this->app->register(LogViewerServiceProvider::class);

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['hackathonstarter'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/hackathonstarter.php' => config_path('hackathonstarter.php'),
        ], 'hackathonstarter.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/henrieri'),
        ], 'hackathonstarter.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/henrieri'),
        ], 'hackathonstarter.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/henrieri'),
        ], 'hackathonstarter.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}

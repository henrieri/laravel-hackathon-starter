<?php

namespace Henrieri\HackathonStarter\Services;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class ResponseBuilder
{

    const STATUS_ERROR = 'error';
    const STATUS_OK = 'ok';

    private $entities;
    private $pivot;
    private $misc;
    private $status = self::STATUS_OK;
    private $errors;

    private $statusCode = Response::HTTP_OK;

    public function addEntity(Model $entity, $key = 'id')
    {
        $table = $entity->getTable();


        foreach($entity->getRelations() as $relation) {
        }

        if (!isset($this->entities[$table])) {
            $this->entities[$table] = [];
        }


        $id = $entity->{$key};


        if (isset($this->entities[$id])) {
            foreach ($entity->toArray() as $key => $value) {
                $this->entities[$id][$key] = $value;
            }
        } else {
            $this->entities[$table][$id] = $entity->toArray();
        }

        return $this;
    }

    public function addEntities($entities, $key = 'id')
    {
        foreach ($entities as $entity) {
            $this->addEntity($entity, $key);
        }

        return $this;
    }

    public function addMisc($data)
    {
        foreach ($data as $key => $value) {
            $this->misc[$key] = $value;
        }

        return $this;
    }

    public function getProperties()
    {
        return ['entities', 'pivot', 'misc', 'status', 'errors'];
    }

    public function toResponse()
    {
        $response = [];

        foreach ($this->getProperties() as $row) {

            if ($this->{$row}) {
                $response[$row] = $this->{$row};
            }
        }

        return response()->json($response, $this->statusCode);
    }

    public function addAccessToken($accessToken)
    {
        return $this->addMisc(['accessToken' => $accessToken]);
    }

    public function getMisc($key = false, $default = null)
    {
        if (!$key) {
            return $this->misc;
        }

        if (!isset($this->misc[$key])) {
            return $default;
        }

        return $this->misc[$key];
    }

    public function render()
    {
        return $this->toResponse();
    }

    public function reset()
    {

        foreach ($this->getProperties() as $row) {

            if ($this->{$row}) {
                $response[$row] = null;
            }
        }
    }

    /**
     * @param $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function getProperty($property, $default)
    {
        if (!isset($this->{$property})) {
            return $default;
        }

        return $this->{$property};
    }

    public function addError($error)
    {

        $this->errors = array_merge($this->getProperty('errors', []), [$error]);

        $this->status = self::STATUS_ERROR;

        return $this;
    }


}

<?php

namespace Henrieri\HackathonStarter\Facades;

use Illuminate\Support\Facades\Facade;

class HackathonStarter extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'hackathonstarter';
    }
}

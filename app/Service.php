<?php

namespace App;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded = [];

    public function toArray()
    {
        $array = parent::toArray();

        $array['id'] = $this->public_id;

        return $array;
    }
}

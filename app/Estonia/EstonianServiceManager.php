<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 13.12.18
 * Time: 11:45
 */

namespace App\Estonia;


use App\Service;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use ReflectionClass;
use Symfony\Component\Finder\Finder;

class EstonianServiceManager
{

    protected $services = [];
    private $user;

    public function __construct(User $user)
    {


        $this->user = $user;
        $this->load(__DIR__.'/Services');
    }

    public function getServices()
    {
        return $this->services;
    }

    public function load($paths)
    {
        $paths = array_unique(Arr::wrap($paths));

        $paths = array_filter($paths, function ($path) {
            return is_dir($path);
        });


        $namespace = app()->getNamespace();

        foreach ((new Finder)->in($paths)->files() as $service) {
            $service = $namespace.str_replace(
                    ['/', '.php'],
                    ['\\', ''],
                    Str::after($service->getPathname(), app_path().DIRECTORY_SEPARATOR)
                );


            $this->register($service);
        }
    }

    public function register($service)
    {

        $instance = new $service($this->user);

        if (!$instance->getName() || $instance->getPriority() < 1) {
            return;
        }

        $this->services[] = new Service($instance->toArray());
    }
}

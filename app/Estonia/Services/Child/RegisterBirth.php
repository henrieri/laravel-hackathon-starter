<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 13.12.18
 * Time: 0:22
 */

namespace App\Estonia\Services\Child;


class RegisterBirth extends BaseChildService
{
    protected $name = 'Sünni registreerimine';

    public function doCalculations()
    {

        if ($this->childMatch(function($child) {
            return $child->getAge() >= 0;
        })) {
            $this->priority = 100;
        }
    }
}

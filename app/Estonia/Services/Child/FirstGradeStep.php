<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 13.12.18
 * Time: 0:45
 */

namespace App\Estonia\Services\Child;


class FirstGradeStep extends BaseChildService
{
    protected $name = '1. klassi astumine';

    public function doCalculations()
    {

        $this->status = 'future';

        $children = $this->user->children;

        $children = $children->filter(function($child) {
           return $child->second->getAge() > 5;
        });

        if (count($children) > 0) {
            $this->priority = 80;

            $child = $children->first();

            $this->link = '/kid/'.$child->id;
        } else {
            $this->priority = 0;
        }

    }

}

<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 13.12.18
 * Time: 0:46
 */

namespace App\Estonia\Services\Child;


class KindergartenSpot extends BaseChildService
{

    protected $name = 'Leia lasteaiakoht';

    public function doCalculations()
    {


        if ($this->childMatch(function($child) {
            return $child->getAge() > 1;
        })) {
            $this->priority = 80;
        } else  {

            if ($this->childMatch(function($child) {
                return $child->getAge() <= 1;
            })) {
                $this->priority = 40;
                $this->status = 'future';
            }
        }
    }

}

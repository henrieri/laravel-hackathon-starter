<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 13.12.18
 * Time: 0:46
 */

namespace App\Estonia\Services\Child;


class StudentSupport extends BaseChildService
{
    protected $name = 'Õpilase toetus';

    public function doCalculations()
    {

        if ($this->getChildren()->count() < 1) {
            return;
        }

        $this->status = 'future';
        $this->priority = 20;

        if ($this->childMatch(function($child) {
            return $child->getAge() > 3;
        })) {
            $this->priority = 60;
        }
        else if ($this->childMatch(function($child) {
            return $child->getAge() < 3;
        })) {
            $this->priority = 70;
            $this->status = 'future';
        }
    }
}

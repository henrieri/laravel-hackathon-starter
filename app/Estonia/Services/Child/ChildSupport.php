<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 13.12.18
 * Time: 0:45
 */

namespace App\Estonia\Services\Child;


class ChildSupport extends BaseChildService
{
    protected $name = 'Määra lastetoetuste saajad';

    public function doCalculations()
    {
        if ($this->getChildren()->count() < 1) {
            return;
        }

        $child = $this->getChildren()->first();

        $this->priority = 60;
        $this->link = '/kid/'.$child->id;
    }
}

<?php

namespace App\Estonia\Services\Child;


use App\Estonia\Services\BaseService;

class BaseChildService extends BaseService
{

    protected $category = 'family';
    protected $image = 'lapsesynd.svg';

    public function setLink()
    {

        $child = $this->getChildren()->first();

        if (!$child) {
            return;
        }

        $this->link = '/kid/'.$child->id;
    }

    public function doCalculations()
    {
        if ($this->getChildren()->count() < 1) {
            return;
        }
        $this->priority = 60;
    }

    public function getChildren()
    {
        return $this->user->children;
    }

    public function childMatch($callback) {
        return $this->getChildren()->filter( function($child) use ($callback) {

            $child = $child->second;

            return $callback($child);
        })->count() > 0;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 13.12.18
 * Time: 0:45
 */

namespace App\Estonia\Services\Child;


class BirthSupport extends BaseChildService
{
    protected $name = 'Sünnitoetus';

    public function doCalculations()
    {

        $this->getChildren();

        if ($this->childMatch( function($child) {
            return $child->getAge() < 3;
        })) {
            $this->priority = 80;
        } else {
            $this->priority = 0;
        }
    }
}

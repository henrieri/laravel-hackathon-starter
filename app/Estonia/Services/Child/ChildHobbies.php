<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 13.12.18
 * Time: 0:47
 */

namespace App\Estonia\Services\Child;


class ChildHobbies extends BaseChildService
{
    protected $name = 'Leia lapsele hobid';

    public function doCalculations()
    {

        if ($this->childMatch(function($child) {
            return $child->getAge() > 3;
        })) {
            $this->priority = 60;
        }
        else if ($this->childMatch(function($child) {
            return $child->getAge() < 3;
        })) {
            $this->priority = 70;
            $this->status = 'future';
        }
    }
}

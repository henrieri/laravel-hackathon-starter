<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 13.12.18
 * Time: 21:36
 */

namespace App\Estonia\Services\CarOwner;


use App\Estonia\Services\BaseService;

class BaseCarOwner extends BaseService
{
    protected $category = 'car-owner';
    protected $image = 'auto.svg';
}

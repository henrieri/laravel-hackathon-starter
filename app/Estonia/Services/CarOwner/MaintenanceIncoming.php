<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 13.12.18
 * Time: 0:47
 */

namespace App\Estonia\Services\CarOwner;


use App\Estonia\Services\BaseService;

class MaintenanceIncoming extends BaseCarOwner
{
    protected $name = 'Tehnoülevaatus';
    protected $image = 'dokument.svg';

    public function doCalculations()
    {

        if ($this->user->vehicles->count() < 1) {
            return;
        }

        $this->priority = 80;

    }
}

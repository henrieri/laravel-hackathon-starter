<?php

namespace App\Estonia\Services;


use App\User;

class BaseService
{

    protected $id = 0;
    protected $user;
    protected $priority;
    protected $name = null;
    protected $category = 'Baasteenus';
    protected $description = 'Kirjeldus';
    protected $public_id;
    protected $status = 'now';
    protected $link = false;

    public function __construct(User $user)
    {
        $this->user = $user;
        $id = str_replace('\\', '-', get_class($this));

        $id = str_replace('App-Estonia-Services-', '', $id);
        $this->public_id = $id;

        $this->doCalculations();
        
        $this->setLink();
    }

    public function setLink()
    {
        
    }

    public function doCalculations()
    {
        $this->priority = 0;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'priority' => $this->priority,
            'category' => $this->category,
            'public_id' => $this->public_id,
            'description' => $this->description,
            'status' => $this->status,
            'image' => $this->image,
            'link' => $this->link
        ];
    }

    public function getId()
    {
        return get_class($this);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPriority()
    {
        return $this->priority;
    }
}

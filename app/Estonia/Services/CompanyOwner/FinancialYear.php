<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 13.12.18
 * Time: 0:48
 */

namespace App\Estonia\Services\CompanyOwner;


class FinancialYear extends CompanyBaseService
{
    protected $name = 'Esita majandus aasta aruanne';


    public function doCalculations()
    {

        if ($this->user->companies->count() < 1) {
            return;
        }

        $this->priority = 80;

    }
}

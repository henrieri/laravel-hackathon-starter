<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 13.12.18
 * Time: 14:04
 */

namespace App\Estonia\Services\CompanyOwner;


use App\Estonia\Services\BaseService;

class CompanyBaseService extends BaseService
{
    protected $category = 'company';
    protected $image = 'dokument.svg';
}

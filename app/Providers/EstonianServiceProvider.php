<?php

namespace App\Providers;

use App\Estonia\EstonianServiceManager;
use Illuminate\Support\ServiceProvider;

class EstonianServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(EstonianServiceManager::class, function() {

           return new EstonianServiceManager(user());
        });
    }
}

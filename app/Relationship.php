<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relationship extends Model
{
    protected $guarded = [];

    public function first()
    {
        return $this->belongsTo(User::class, 'first_id');
    }

    public function second()
    {
        return $this->belongsTo(User::class, 'second_id');
    }
}

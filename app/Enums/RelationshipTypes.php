<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 13.12.18
 * Time: 0:51
 */

namespace App\Enums;


class RelationshipTypes
{
    const FATHER = 'father';
    const MOTHER = 'mother';
    const MARRIED = 'married';
}

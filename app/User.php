<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;
use Plank\Metable\Metable;

class User extends Authenticatable
{
    use Notifiable, Metable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function companies()
    {
        return $this->belongsToMany(Company::class);
    }

    public function relationships()
    {
        return $this->hasMany(Relationship::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    public function children()
    {
        return $this->hasMany(Relationship::class, 'first_id')->whereIn('relationship_type', [
            'father', 'mother'
        ]);
    }

    public function getBirthDate()
    {
        $idCode = $this->identity_code;

        if (!$idCode) {
            return now()->addYear(1);
        }
        $birthYear = $idCode[0] > 4 ? 2000 : 1900;
        $birthYear = $birthYear + (int)($idCode[1] . $idCode[2]);
        $month = (int)($birthYear[3] . $birthYear[4]);
        $day = (int)($birthYear[5] . $birthYear[6]);

        $birthDate = Carbon::parse($birthYear . '-' . $month . '-' . $day);
        return $birthDate;
    }

    public function getAge()
    {
        $birthDate = $this->getBirthDate();
        $currentDate = now();
        return abs($currentDate->diffInYears($birthDate));
    }

    public function generateIdentityCode()
    {
        $expectedBirthdate = Carbon::parse($this->expected_birth_date);

        $idCode = 6 . $expectedBirthdate->year - 2000 . $expectedBirthdate->format('md') . rand(1000, 9999);

        $this->identity_code = $idCode;
        $this->save();
    }


    public function toArray()
    {
        $array = parent::toArray();

        $array['age'] = $this->getAge();
        $array['birth_date'] = $this->getBirthDate()->format('Y-m-d');

        return $array;
    }
}

<?php

namespace App\Exceptions;


class InvalidRequestException extends BaseException
{
    const INVALID_LOGIN_CREDENTIALS = 1;
    const SOCIAL_ACCOUNT_DOES_NOT_EXIST = 2;
    const NOT_LOGGED_IN = 3;

    public function __construct($code)
    {
        $this->code = $code;
    }

    public function getErrorArray() {

        return array_merge($this->errorDefinitionArrays()[$this->getCode()], [
            'errorCode' => $this->getCode()
        ]);
    }

    public function errorDefinitionArrays()
    {
        return [
            self::INVALID_LOGIN_CREDENTIALS => [
                'message' => 'Invalid login credentials',
                'slug' => 'invalid-login-credentials',
                'description' => 'We could not find a row matching those login credentials in our database'
            ]
        ];
    }
}

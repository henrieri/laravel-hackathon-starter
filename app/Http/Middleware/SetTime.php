<?php

namespace App\Http\Middleware;

use App\Site;
use Closure;
use Illuminate\Support\Carbon;

class SetTime
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $site = Site::firstOrCreate([
            'key' => 'time'
        ]);

        if ($site->value) {
            Carbon::setTestNow(Carbon::parse($site->value));
        }


        return $next($request);
    }
}

<?php

namespace App\Http\Controllers;

use App\Kindergarten;
use Illuminate\Http\Request;

class KindergartenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kindergarten  $kindergarten
     * @return \Illuminate\Http\Response
     */
    public function show(Kindergarten $kindergarten)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kindergarten  $kindergarten
     * @return \Illuminate\Http\Response
     */
    public function edit(Kindergarten $kindergarten)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kindergarten  $kindergarten
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kindergarten $kindergarten)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kindergarten  $kindergarten
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kindergarten $kindergarten)
    {
        //
    }
}

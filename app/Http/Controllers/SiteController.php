<?php

namespace App\Http\Controllers;

use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class SiteController extends Controller
{
    public function index()
    {

        $sites = Site::all();

        $site = $sites->where('key', 'time')->first();

        if (!$site) {
            $site = new Site([
                'id' => 0,
                'key' => 'time',
                'value' => now()->toDateString()
            ]);
            $sites->push($site);
        }

        $site->value = Carbon::parse($site->value)->timestamp * 1000;

        return $this->getResponseBuilder()->addEntities($sites)->toResponse();
    }
}

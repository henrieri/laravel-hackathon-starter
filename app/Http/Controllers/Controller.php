<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidRequestException;
use App\User;
use Henrieri\HackathonStarter\Services\ResponseBuilder;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function error($errorCode = 0)
    {
        throw new InvalidRequestException($errorCode);
    }

    public function getResponseBuilder(): ResponseBuilder
    {
        return resolve(ResponseBuilder::class);
    }

    public function userResponse(User $user)
    {
        $accessToken = $user->createToken('User Token')->accessToken;

        return $this->getResponseBuilder()->addEntity($user)->addAccessToken($accessToken)->toResponse();
    }

}

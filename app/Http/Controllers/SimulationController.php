<?php

namespace App\Http\Controllers;

use App\Event;
use App\Relationship;
use App\Site;
use App\User;
use Illuminate\Http\Request;use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;

class SimulationController extends Controller
{
    public function simulate(Request $request)
    {


        if ($request->simulation === 'birth') {
            $child = User::create([
                'expected_birth_date' => now()->subMonth()->addMonths(9),
                'api_token' => str_random(60)
            ]);

            Relationship::create([
                'first_id' => user()->id,
                'second_id' => $child->id,
                'relationship_type' => 'mother'
            ]);

            return;
        }

        if ($request->simulation === 'time') {
            $site = Site::firstOrCreate([
                'key' => 'time'
            ]);
            $site->value = Carbon::parse($request->value);
            $site->save();

            $eventArray = [[
                now()->toDateTimeString(), 'Lapse sünd', 'lapsesynd.svg',
            ],
                [
                    now()->addMonth()->toDateTimeString(), 'Külasta lapsega perearsti', 'lapsesynd.svg',
                ],
                [
                now()->addYears(1), 'Vanemahüvitis lõpeb', 'lapsesynd.svg',
            ],
            [
                now()->addYears(2), 'Laps läheb lasteaeda', 'lapsesynd.svg',
            ]

            ];

            $children = User::where('identity_code', null)->get();

            $children->each( function($child) {
                $child->generateIdentityCode();
            });

            foreach($eventArray as $event) {

                factory(Event::class)->create([
                    'event_started_at' => Carbon::createFromFormat('Y-m-d H:i:s', $event[0])->toDateString(),
                    'event_name' => $event[1],
                    'image' => $event[2],
                    'user_id' => user()->id,
                ]);
            }


            return response('');
        }

        if ($request->simulation === 'reset') {
            Artisan::call('migrate:fresh');
            Artisan::call('db:seed');
        }

    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Plank\Metable\Meta;

class MetaController extends Controller
{
    public function index()
    {
        return $this->getResponseBuilder()->addEntities(Meta::all())->render();
    }

    public function updateMeta(Request $request)
    {
        $user = User::find($request->id);

        $user->setMeta($request->key, $request->value);

        return $this->getResponseBuilder()->addEntities(Meta::all())->render();
    }
}

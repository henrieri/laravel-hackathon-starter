<?php

use App\Company;
use App\Enums\RelationshipTypes;
use App\Relationship;
use App\User;
use App\Vehicle;
use App\Event;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);


        $father = factory(User::class)->create([
            'identity_code' => '39305163560',
            'first_name' => 'Olari',
            'last_name' => 'Tõnison',
        ]);

        $mother = factory(User::class)->create([
            'identity_code' => '49405163560',
            'first_name' => 'Sandra',
            'last_name' => 'Tõnison',
        ]);

        $baby = factory(User::class)->create([
            'identity_code' => '61805163560',
            'first_name' => null,
            'last_name' => 'Tõnison',
        ]);

        $father->syncMeta([
            'address' => 'Test aadress',
        ]);

        factory(Relationship::class)->create([
            'first_id' => $father->id,
            'second_id' => $mother->id,
            'relationship_type' => RelationshipTypes::MARRIED
        ]);

        factory(Relationship::class)->create([
            'first_id' => $mother->id,
            'second_id' => $baby->id,
            'relationship_type' => RelationshipTypes::MOTHER,
        ]);

        $company = factory(Company::class)->create([
           'company_name' => 'Optimus Digitalis OÜ'
        ]);

        $father->companies()->attach($company->id);

        $almostMother = factory(User::class)->create([
            'first_name' => 'Mari',
            'last_name' => 'Madalvee',
            'identity_code' => '49445554444'
        ]);

        $vehicle = factory(Vehicle::class)->create([
            'user_id' => $almostMother->id
        ]);

        $eventArray = [
            [
                '2017-06-20 00:00:00', 'Ülikooli lõpetamine', 'ylikool.svg',
            ],
            [
                '2017-06-20 00:00:00', 'Uus elukoht', 'elukoht.svg',
            ],
            [
                '2017-08-08 00:00:00', 'Abielu', 'abielu.svg',
            ],
            [
                '2017-08-19 00:00:00', 'Autojuhiload', 'juhiload.svg',
            ],

      /*      [
                '2018-12-22 00:00:00', 'Pass aegub', 'dokument.svg',
            ],
            [
                '2019-10-22 00:00:00', 'ID-kaart aegub', 'dokument.svg',
            ],
           ,*/
        ];

        foreach($eventArray as $event) {

            foreach([$father, $mother, $baby, $almostMother] as $usera) {
                factory(Event::class)->create([
                    'event_started_at' => Carbon::createFromFormat('Y-m-d H:i:s', $event[0])->toDateString(),
                    'event_name' => $event[1],
                    'image' => $event[2],
                    'user_id' => $usera->id,
                ]);
            }
        }
    }
}

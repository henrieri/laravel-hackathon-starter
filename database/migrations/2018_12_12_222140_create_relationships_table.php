<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relationships', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('first_id');
            $table->foreign('first_id')->references('id')->on('users');

            $table->unsignedInteger('second_id');
            $table->foreign('second_id')->references('id')->on('users');

            $table->string('relationship_type');

            $table->text('description')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relationships');
    }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->group(function() {
    Route::resource('doctors', 'DoctorController');
    Route::resource('events', 'EventController');
    Route::resource('kindergartens', 'KindergartenController');
    Route::resource('relationships', 'RelationshipController');
    Route::resource('services', 'ServiceController');
    Route::resource('vehicles', 'VehicleController');
    Route::post('meta/update', 'MetaController@updateMeta');
    Route::post('simulation', 'SimulationController@simulate');
});

Route::resource('users', 'UserController');

Route::resource('meta', 'MetaController');
Route::resource('sites', 'SiteController');
Route::post('login', 'LoginController@login');
